<?php

class ModelTests extends BaseTests
{
    /**
     * Testing insertion of a book
     */
    public function testBookInsert()
    {
        // There is no book
        $books = $this->app['model']->getBooks();
        $this->assertEquals(0, count($books));

        // Inserting one
        $idBook=$this->app['model']->insertBook('Test', 'Someone', 'A test book', 'image', 3);

        // There is one book
        $books = $this->app['model']->getBooks();
        $this->assertEquals(1, count($books));

        // TODO: Vérifier que 3 exemplaires ont été créés
        $this->assertEquals(3,$this->app['model']->getNbExemplaireBook($idBook));

        $client = $this->createClient();
         $this->app['session']->set('admin', true);

        // $crawler = $client->request('POST', '/admin', ['login' => 'admin', 'password' => 'password']);
        $crawler = $client->request('POST', '/emprunter/1/1',['nom_emprunteur'=> 'test' ,'date_fin'=> '2017-10-12']);
        
        // $crawler = $client->request('get', '/emprunter/1/1');
        // $form = $crawler->filter('form')->form();
        // $form['nom_emprunteur'] = 'Test';
        // $form['date_fin'] = date('d/m/y');
        // $client->submit($form);

        $book=$this->app['model']->getBookWithExemplaires($idBook);
        $this->assertEquals(2,count($book["exemplairesDispo"]));

        $crawler = $client->request('POST', '/rendre/1/1');
        $book=$this->app['model']->getBookWithExemplaires($idBook);
        $this->assertEquals(3,count($book["exemplairesDispo"]));


    }
}
