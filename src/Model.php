<?php

class Model
{
    protected $pdo;

    public function __construct(array $config)
    {
        try {
            if ($config['engine'] == 'mysql') {
                $this->pdo = new \PDO(
                    'mysql:dbname='.$config['database'].';host='.$config['host'],
                    $config['user'],
                    $config['password']
                );
                $this->pdo->exec('SET CHARSET UTF8');
            } else {
                $this->pdo = new \PDO(
                    'sqlite:'.$config['file']
                );
            }
        } catch (\PDOException $error) {
            throw new ModelException('Unable to connect to database');
        }
    }

    /**
     * Tries to execute a statement, throw an explicit exception on failure
     */
    protected function execute(\PDOStatement $query, array $variables = array())
    {
        if (!$query->execute($variables)) {
            $errors = $query->errorInfo();
            throw new ModelException($errors[2]);
        }

        return $query;
    }

    /**
     * Inserting a book in the database
     */
    public function insertBook($title, $author, $synopsis, $image, $copies)
    {
        $query = $this->pdo->prepare('INSERT INTO livres (titre, auteur, synopsis, image)
            VALUES (?, ?, ?, ?)');
        $this->execute($query, array($title, $author, $synopsis, $image));

        $idBook=$this->pdo->lastInsertId();

        for ($i=0; $i < $copies; $i++) { 
            $query = $this->pdo->prepare('INSERT INTO exemplaires (book_id) values (?) ');
            $this->execute($query, array($idBook));
        }
        return $idBook;
    }

    /**
     * Getting all the books
     */
    public function getBooks()
    {
        $query = $this->pdo->prepare('SELECT livres.* FROM livres');

        $this->execute($query);

        return $query->fetchAll();
    }

    /**
     * Getting all the books
     */
    public function getBook($idLivre)
    {
        // $query = $this->pdo->prepare('SELECT livres.*,count(exemplaires.id) as exemplaires FROM livres left join exemplaires on exemplaires.book_id=livres.id where livres.id= ?');
        $query = $this->pdo->prepare('SELECT livres.* FROM livres where livres.id= ?');

        $this->execute($query,array($idLivre));

        return $query->fetch(PDO::FETCH_ASSOC);
    }

    public function getNbExemplaireBook($idLivre)
    {
        $query = $this->pdo->prepare('SELECT count(*) FROM livres inner join exemplaires on exemplaires.book_id=livres.id where livres.id like ?');

        $this->execute($query,array($idLivre));

        return $query->fetchColumn();
    }
    
    public function isExemplaireDisponible($idExemplaire){


        $query=$this->pdo->prepare('SELECT CASE WHEN ( emprunts.fini IS NULL or emprunts.fini = 1 ) THEN 1 ELSE 0 END as disponible '.
        'from exemplaires '.
        'left join emprunts on emprunts.exemplaire=exemplaires.id '.
        'where  '.
        'exemplaires.id like ? '.
        'order by debut desc '.
        'limit 1 ');

        // var_dump($query);
        $this->execute($query,array($idExemplaire));

        $result=$query->fetch(PDO::FETCH_ASSOC)["disponible"];
        // var_dump($result);
        return $result;
        // return $query->fetch(PDO::FETCH_ASSOC)[disponible];

    }
    public function getBookWithExemplaires($idLivre)
    {   
        $book=$this->getBook($idLivre);
        // var_dump($book);
        if($book){

            $query = $this->pdo->prepare('SELECT exemplaires.id '.
            ' FROM livres '.
            ' inner join exemplaires on exemplaires.book_id=livres.id'.
            ' where livres.id'.
            ' like ?');

            // var_dump($query);
            $this->execute($query,array($idLivre));

            $exemplaires=$query->fetchAll();

            $exemplairesDispo=array();
            $exemplairesPris=array();
            foreach ($exemplaires as $exemplaire) 
            {

                if($this->isExemplaireDisponible($exemplaire['id']))
                    $exemplairesDispo[]=$exemplaire;
                else
                    $exemplairesPris[]=$exemplaire;
            }
            $book['exemplairesDispo']=$exemplairesDispo;
            $book['exemplairesPris']=$exemplairesPris;

            return $book;
        }
        else
        {
            return false;
        }
    }
    public function emprunterExemplaire($exemplaire,$nom,$datefin)
    {   
        $query = $this->pdo->prepare('INSERT INTO `emprunts` (`personne`, `exemplaire`, `debut`, `fin`, `fini`) VALUES  (?,?,"NOW()",?,0);');
        
        $this->execute($query,array($nom,$exemplaire,$datefin));
    }

    public function rendreExemplaire($exemplaire)
    {   
        $query = $this->pdo->prepare('UPDATE emprunts SET fini = 1 , fin = "NOW()" WHERE exemplaire=?');
        $result=$this->execute($query, array($exemplaire));
    }

    // public function getExemplaireWithBook($idExemplaire)
    // {   
    //     $book=$this->getBook($idLivre);
        
    //     $query = $this->pdo->prepare('SELECT exemplaires.*,'.
    //     ' CASE WHEN ( emprunts.fini IS NULL or emprunts.fini = 1 ) THEN true ELSE false END as disponible'.
    //     ' FROM livres '.
    //     ' inner join exemplaires on exemplaires.book_id=livres.id'.
    //     ' left join emprunts on emprunts.exemplaire=exemplaires.id'.
    //     ' where livres.id'.
    //     ' like ?');

    //     $this->execute($query,array($idLivre));

    //     $book['listExemplaires']=$query->fetchAll();

    //     return $book;
    // }


} 
