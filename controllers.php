<?php

use Gregwar\Image\Image;

$app->match('/', function() use ($app) {
    return $app['twig']->render('home.html.twig');
})->bind('home');

$app->match('/books', function() use ($app) {
    return $app['twig']->render('books.html.twig', array(
        'books' => $app['model']->getBooks()
    ));
})->bind('books');

$app->get('/book/{id}', function($id) use ($app) {
    
    $book=$app['model']->getBookWithExemplaires($id);
    // var_dump($book);
    if($book!=false){
        return $app['twig']->render('book.html.twig', array(
        'book' => $book
        ));
    }
    else{
        $app->abort(404, "Livre $id does not exist.");
    }
    
})->bind('book');

$app->match('/emprunter/{book}/{exemplaire}', function($book,$exemplaire) use ($app) {
    
    if (!$app['session']->has('admin')) {
        return $app['twig']->render('shouldBeAdmin.html.twig');
    }
    $request = $app['request'];
   // $book=$app['model']->getBookWithExemplairesStatus($book);
    if ($request->getMethod() == 'POST')
    {
        $post = $request->request;
        
        if($post->has('nom_emprunteur') && $post->has('date_fin')){

            $nom=$post->get('nom_emprunteur');
            $datefin=$post->get('date_fin');
            
            $app['model']->emprunterExemplaire($exemplaire,$nom,$datefin);

            return $app->redirect($app['url_generator']->generate('book',array('id' => $book)));
            // return $app['twig']->render('book.html.twig', array(
            //     'book' => $book
            // ));
            
        }
        //$app['model']->emprunterExemplaire();
    }
    else{
        $book=$app['model']->getBookWithExemplaires($book);
        return $app['twig']->render('emprunter.html.twig', array(
            'exemplaire' => $exemplaire,
            'book' => $book
        ));

    } 
    
})->bind('emprunter');

$app->post('/rendre/{book}/{exemplaire}', function($book,$exemplaire) use ($app) {
    
    if (!$app['session']->has('admin')) {
        return $app['twig']->render('shouldBeAdmin.html.twig');
    }
    // var_dump("salut");
    $app['model']->rendreExemplaire($exemplaire);
    return $app->redirect($app['url_generator']->generate('book',array('id' => $book)));
    
})->bind('rendre');

// $app->post('/emprunter/{id}', function($id) use ($app) {
    
//     $book=$app['model']->getBookWithExemplairesStatus($id);
//     // var_dump($book);
//     if($book!=false){
//         return $app['twig']->render('emprunter.html.twig', array(
//         'book' => $book
//         ));
//     }
//     else{
//         $app->abort(404, "Livre $id does not exist.");
//     }
    
// })->bind('emprunter');



$app->match('/admin', function() use ($app) {
    $request = $app['request'];
    $success = false;
    if ($request->getMethod() == 'POST') {
        $post = $request->request;
        if ($post->has('login') && $post->has('password')) 
        {

            foreach ($app['config']['admin'] as $admin) {
                if(array($post->get('login'), $post->get('password'))== $admin)
                {
                    $success = true;
                }
            }
            
            if($success==true)
                $app['session']->set('admin', true);
            
        }
    }
    return $app['twig']->render('admin.html.twig', array(
        'success' => $success
    ));
})->bind('admin');

$app->match('/logout', function() use ($app) {
    $app['session']->remove('admin');
    return $app->redirect($app['url_generator']->generate('admin'));
})->bind('logout');

$app->match('/addBook', function() use ($app) {
    if (!$app['session']->has('admin')) {
        return $app['twig']->render('shouldBeAdmin.html.twig');
    }

    $request = $app['request'];
    if ($request->getMethod() == 'POST') {
        $post = $request->request;
        if ($post->has('title') && $post->has('author') && $post->has('synopsis') &&
            $post->has('copies')) {
            $files = $request->files;
            $image = '';

            // Resizing image
            if ($files->has('image') && $files->get('image')) {
                $image = sha1(mt_rand().time());
                Image::open($files->get('image')->getPathName())
                    ->resize(240, 300)
                    ->save('uploads/'.$image.'.jpg');
                Image::open($files->get('image')->getPathName())
                    ->resize(120, 150)
                    ->save('uploads/'.$image.'_small.jpg');
            }

            // Saving the book to database
            $app['model']->insertBook($post->get('title'), $post->get('author'), $post->get('synopsis'),
                $image, (int)$post->get('copies'));
        }
    }

    return $app['twig']->render('addBook.html.twig');
})->bind('addBook');

